var cordova = require('cordova');

var afvideo = {
	play: function(options, successFunction, errorFunction) {
		cordova.exec(successFunction, errorFunction, "afvideo", "play", [options]);
	}
};

module.exports = afvideo;