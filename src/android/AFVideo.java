package biz.appform.video;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;

public class AFVideo extends CordovaPlugin {
    private static final String TAG = AFVideo.class.getSimpleName();
    private static final String ACTION_PLAY_VIDEO = "play";

    private static final int PLAY_REQ = 340;

    private CallbackContext mCallbackContext;

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        JSONObject options = args.optJSONObject(0);

        if( ACTION_PLAY_VIDEO.equals(action) ){
            startVideoActivity( options );
        } else {
            return false;
        }
        return true;
    }

    private void startVideoActivity( JSONObject options ){
        String videoSource = options.optString("videoSource", null);

        Intent intent = new Intent(cordova.getActivity(), AFVideoPlayerActivity.class);
        intent.putExtra("videoSource", videoSource);

        cordova.startActivityForResult(this, intent, PLAY_REQ );
    }

    public void onActivityResult(int requestCode, int resultCode, final Intent intent) {
        if( requestCode == PLAY_REQ){

        }
    }

}