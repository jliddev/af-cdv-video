#import <Cordova/CDVPlugin.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface AFVideo : CDVPlugin

-(void)play:(CDVInvokedUrlCommand*)command;

@end
