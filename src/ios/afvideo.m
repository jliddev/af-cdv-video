#import "afvideo.h"
#import "CDVFile.h"
#import <Cordova/CDVAvailability.h>

@implementation AFVideo

- (void)pluginInitialize
{
}

- (void) play:(CDVInvokedUrlCommand *)command
{
    NSString* callbackId = command.callbackId;
    NSDictionary* options = [command argumentAtIndex:0];
    
    NSURL* url = [NSURL URLWithString:options[@"videoSource"]];
    AVPlayerViewController* ctrl = [[AVPlayerViewController alloc] init];
    AVPlayer* player = [AVPlayer playerWithURL:url];

    ctrl.player = player;
    
    [self.viewController  presentViewController:ctrl animated:YES completion:^{
        [ctrl.player play];
    }];
    
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:YES];
    [self.commandDelegate sendPluginResult:result callbackId:callbackId];
}

@end
